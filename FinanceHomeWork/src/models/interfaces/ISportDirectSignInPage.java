package models.interfaces;

public interface ISportDirectSignInPage {
    void goToPasswordRecovery();

    void fillPasswordRecoveryForm(String email);

    void fillNewPassword(String password);

    void openAccount();
}
