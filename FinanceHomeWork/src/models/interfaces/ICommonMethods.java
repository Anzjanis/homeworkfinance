package models.interfaces;

public interface ICommonMethods {

    void checkUrl(String expectedUrl);
    void closePopUp();
}
