package models.interfaces;

public interface ISportDirectHomePage {

    void goToShoeSection();

    void goToSignIn();
}
