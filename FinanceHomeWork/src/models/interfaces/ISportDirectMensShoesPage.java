package models.interfaces;

public interface ISportDirectMensShoesPage {
    void sortByBrands();
    void sortByPriceRange(int minRange, int maxRange);

    void checkIfItemsAreFilteredCorrectly();
}
