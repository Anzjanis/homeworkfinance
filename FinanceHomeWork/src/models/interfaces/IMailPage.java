package models.interfaces;

public interface IMailPage {

    void loginMailOpenFirstMessage(String email, String password);

    void getHrefGoBackToSportDirect();
}
