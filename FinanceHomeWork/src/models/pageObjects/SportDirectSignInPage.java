package models.pageObjects;

import models.interfaces.ICommonMethods;
import models.interfaces.ISportDirectSignInPage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import testManager.java.InitProject;

public class SportDirectSignInPage implements ISportDirectSignInPage {

    @FindBy(xpath="//*[contains(@id, 'LoginScreen_registerLogin_cmdForgottenPassword')]")
    private WebElement clickPasswordRecovery;

    @FindBy(css="[id$=PasswordReset_UserName]")
    private WebElement fillRecoveryForm;

    @FindBy(xpath="//*[contains(@id, 'txtNewPassword')]")
    private WebElement fillNewPassword;

    @FindBy(xpath="//*[contains(@id, 'PasswordReset_cmdSendPassword')]")
    private WebElement passwordResetSand;

    @FindBy(xpath="//*[contains(@id, 'PasswordReset_txtConfirmNewPassword')]")
    private WebElement passwordConfirm;

    @FindBy(xpath="//*[contains(@id, 'PasswordReset_lnkbtnChangePassword')]")
    private WebElement passwordChangeNext;

    @FindBy(xpath="//*[contains(@id, 'ctl00_lblMessage')]")
    private WebElement emailSuccessMessage;

    @FindBy(xpath="//*[contains(@id, 'PasswordReset_SuccessText')]")
    private WebElement successMessage;

    @FindBy(xpath="//*[contains(@id, 'PasswordReset_lnkbtnContinue')]")
    private WebElement clickContinue;

    @FindBy(css="#topLinkMenu > ul > li")
    private WebElement clickAccount;

    public void goToPasswordRecovery()
    {
        PageFactory.initElements(InitProject.getDriver(), this);
        clickPasswordRecovery.click();
    }

    public void fillPasswordRecoveryForm(String email)
    {
        PageFactory.initElements(InitProject.getDriver(), this);
        fillRecoveryForm.sendKeys(email);
        passwordResetSand.click();
        CommonMethods.checkIfElemIsReady(emailSuccessMessage, 5);
    }

    public void fillNewPassword(String password)
    {
        PageFactory.initElements(InitProject.getDriver(), this);
        fillNewPassword.click();
        fillNewPassword.sendKeys(password);
        passwordConfirm.sendKeys(password);
        CommonMethods.clickWhenReady(passwordChangeNext, 10);

        CommonMethods.checkIfElemIsReady(successMessage, 5); //Wait for success message
        clickContinue.click();
    }


    public void openAccount()
    {
        clickAccount.click();
    } //Opens account to make sure, user is logged in after password recovery
}
