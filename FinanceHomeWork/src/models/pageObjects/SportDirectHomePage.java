package models.pageObjects;


import models.interfaces.ISportDirectHomePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import testManager.java.InitProject;

public class SportDirectHomePage implements ISportDirectHomePage {

    @FindBy(css="li.root:nth-child(1) > a:nth-child(1)")
    public WebElement ClickMainMenuMans;

    @FindBy(css="#topMenu > ul > li.mmHasChild.root.multicolumn.MenuGroupA.newDrop > div > ul > li.MensCenter.Center > ul > li:nth-child(4) > ul > li:nth-child(9) > a")
    private WebElement ClickShoes;

    @FindBy(id="dnn_dnnLOGIN_loginLink")
    private WebElement ClickSignIn;


    @Override
    public void goToShoeSection() {
        PageFactory.initElements(InitProject.getDriver(), this);
        CommonMethods.checkIfElemIsReady(ClickMainMenuMans, 5);

        Actions action = new Actions(InitProject.getDriver());
        action.moveToElement(ClickMainMenuMans).perform();  //Move mouse to elem, so drop down appears
        action.build();
        CommonMethods.clickWhenReady(ClickShoes, 5); //Click when element is ready
    }

    public void goToSignIn()
    {
        PageFactory.initElements(InitProject.getDriver(), this);
        ClickSignIn.click();
    }
}
