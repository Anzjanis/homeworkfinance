package models.pageObjects;

import models.interfaces.ICommonMethods;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import testManager.java.InitProject;



public class CommonMethods implements ICommonMethods {

    @FindBy(css="#advertPopup > div > div > div.modal-header > button")
    private WebElement closePopUp;

    //This will check url and wait for page to load
    public void checkUrl(String expectedUrl)
    {
        new WebDriverWait(InitProject.getDriver(), 10).until(
                webDriver -> ((JavascriptExecutor) webDriver).executeScript("return document.readyState").equals("complete"));
        String url = InitProject.getDriver().getCurrentUrl();
        Assert.assertEquals(url, expectedUrl);
    }

    // SomeTimes there is popup and sometimes there is no popup
    public void closePopUp()
    {
        PageFactory.initElements(InitProject.getDriver(), this);

        try {
            clickWhenReady(closePopUp, 5);
        }catch(Exception e) {
            System.out.println("popup didnt show up");
                }
     }

     // WAiting for Javascript to be finished
     public static void waitForJsToFinish()
     {
         new WebDriverWait(InitProject.getDriver(), 10).until(
                 webDriver -> ((JavascriptExecutor) webDriver).executeScript("return document.readyState").equals("complete"));
     }

    public static void clickWhenReady(WebElement locator, int timeout) {
        WebDriverWait wait = new WebDriverWait(InitProject.getDriver(), timeout);
        WebElement element = wait.until(ExpectedConditions.elementToBeClickable(locator));
        element.click();
    }

    public static void checkIfElemIsReady(WebElement locator, int timeout) {
        WebDriverWait wait = new WebDriverWait(InitProject.getDriver(), timeout);
        WebElement element = wait.until(ExpectedConditions.elementToBeClickable(locator));
        element.isEnabled();
    }
}
