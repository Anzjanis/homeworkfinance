package models.pageObjects;

import com.google.common.util.concurrent.Uninterruptibles;
import models.interfaces.IMailPage;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import testManager.java.InitProject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class MailPage implements IMailPage {

    @FindBy(id="inboxfield")
    private WebElement fillEmail;


    @FindBy(xpath="/html/body/a")
    private WebElement mailBody;

    @FindBy(id="inboxpane")
    private WebElement checkIfMailIsReady;




    public void loginMailOpenFirstMessage(String email, String password)
    {
        PageFactory.initElements(InitProject.getDriver(), this);
        fillEmail.sendKeys(email);
        fillEmail.sendKeys(Keys.ENTER);
        CommonMethods.waitForJsToFinish();
        CommonMethods.checkIfElemIsReady(checkIfMailIsReady, 6);
        JavascriptExecutor js = (JavascriptExecutor) InitProject.getDriver();
        js.executeScript("document.getElementsByClassName(\"all_message-item all_message-item-parent cf ng-scope\")[0].click();"); //OPens first mail
    }

    @Override
    public void getHrefGoBackToSportDirect()
    {
        PageFactory.initElements(InitProject.getDriver(), this);
        InitProject.getDriver().switchTo().frame(InitProject.getDriver().findElement(By.id("msg_body"))); //Switch to Iframe to get recovery URL
        String recoveryUrl = mailBody.getText();
        InitProject.getDriver().switchTo().defaultContent();
        InitProject.getDriver().get(recoveryUrl);
        Uninterruptibles.sleepUninterruptibly(10000, TimeUnit.MILLISECONDS);
    }
}
