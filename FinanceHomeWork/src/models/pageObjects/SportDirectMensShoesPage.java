package models.pageObjects;

import com.google.common.util.concurrent.Uninterruptibles;
import models.interfaces.ISportDirectMensShoesPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Sleeper;
import org.testng.Assert;
import testManager.java.InitProject;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class SportDirectMensShoesPage implements ISportDirectMensShoesPage {




    @FindBy(css="#dnn_ctr45667222_ViewTemplate_ctl00_ctl07_lstFilters_productFilterList_0 > div:nth-child(2) > a > span")
    public WebElement brandFiretrap;

    @FindBy(id="txtBrandSearch")
    public WebElement brandSearchField;

    @FindBy(css="#dnn_ctr45667222_ViewTemplate_ctl00_ctl07_lstFilters_productFilterList_0 > div:nth-child(13) > a > span > span")
    public WebElement brandSkechers;

    @FindBy(id="PriceFilterTextEntryMin")
    public WebElement priceRangeMin;

    @FindBy(id="PriceFilterTextEntryMax")
    public WebElement priceRangeMax;

    @FindBy(id="PriceFilterTextEntryApply")
    public WebElement priceRangeApply;



    public void sortByBrands()
    {
        PageFactory.initElements(InitProject.getDriver(), this);
        brandFiretrap.click();
        CommonMethods.waitForJsToFinish();
        brandSearchField.sendKeys("Skechers");
        brandSkechers.click();
    }


    public void sortByPriceRange(int minRange, int maxRange) {
        CommonMethods.waitForJsToFinish();
        priceRangeMin.sendKeys(String.valueOf(minRange));
        priceRangeMax.sendKeys(String.valueOf(maxRange));
        priceRangeApply.click();
        CommonMethods.waitForJsToFinish();
    }

    public void checkIfItemsAreFilteredCorrectly()
    {
        CommonMethods.clickWhenReady(priceRangeMin, 5);
        CommonMethods.waitForJsToFinish();
        //Collect all elements and check if names are Firetrap or Skechers
        List <WebElement> elementsBrand = InitProject.getDriver().findElements(By.className("productdescriptionbrand"));
        Assert.assertFalse(elementsBrand.isEmpty(), "There is no elements displayed");
        for(WebElement element : elementsBrand){

            Assert.assertTrue(element.getText().contains("Firetrap") ||  element.getText().contains("Skechers"));
        }

        List <WebElement> elementsPrice = InitProject.getDriver().findElements(By.className("s-largered"));
        Assert.assertFalse(elementsPrice.isEmpty(), "There is no elements displayed");
        for(WebElement element : elementsPrice){
            int price = Integer.valueOf(element.getText().replaceAll("[^\\d]",""));

            Assert.assertTrue(price >= 3000 && //Convert euro to cents, so there is no problems with comma and dots.
                    price <= 6000);
        }
    }
}
