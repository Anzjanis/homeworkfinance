package testManager.java;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static java.util.concurrent.TimeUnit.SECONDS;

public class InitProject {

    //  private WebDriver driver;
    private static WebDriver driver;


    //Driver configs
    public static WebDriver getDriver() {
        driver.manage().timeouts().implicitlyWait(10, SECONDS);
        driver.manage().timeouts().pageLoadTimeout(10, SECONDS);
        driver.manage().window().maximize();

        return driver;
    }

    public static void setDriver(String browser) {

        switch (browser) {
            case "Chrome":
                InitProject.driver = new ChromeDriver();
                break;
            case "PhantomJs":  //ETC;
                break;
            default:
                InitProject.driver = new ChromeDriver();
                break;
        }
    }


    public static void closeBrowser() {
        // Close all browser windows and safely end the session
        driver.quit();
    }
}
