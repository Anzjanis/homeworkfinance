package test;

import models.interfaces.*;
import models.pageObjects.*;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;
import testManager.java.InitProject;


public class SportDirectTests {

    private WebDriver driver;
    private String webPageUrl = "https://lv.sportsdirect.com/"; //HomePage
    private String gmailUrl = "https://www.mailinator.com/"; //Email page

    private ICommonMethods commonMethods = new CommonMethods();
    private ISportDirectHomePage sportDirectHomePage = new SportDirectHomePage();
    private ISportDirectMensShoesPage sportDirectMensShoesPage = new SportDirectMensShoesPage();
    private ISportDirectSignInPage sportDirectSignInPage = new SportDirectSignInPage();
    private IMailPage mailPage = new MailPage();

    @BeforeClass // Runs this method before the first test method in the current class is invoked
    public void setUp() {
        String browserName = "Chrome";
        InitProject.setDriver(browserName);
        driver = InitProject.getDriver();
    }

    //This allow to use same testcase with different values, ex, different mail/password
    @DataProvider(name = "EmailInfo")
    public static Object[][] passwordRecoveryInfo() {

        return new Object[][] { { "gundars4financehomework@mailinator.com", "4finance" }};
    }

    @Test // Marking this method as part of the test
    public void checkIfMansShoesSortingWorksCorrectly()
    {
        driver.get(webPageUrl);
        commonMethods.checkUrl(webPageUrl); //Checks if URL ir correct
        commonMethods.closePopUp(); // Removes popup if it appears
        sportDirectHomePage.goToShoeSection();
        commonMethods.checkUrl(webPageUrl + "mens/mens-shoes");
        sportDirectMensShoesPage.sortByBrands(); //Collects correct brands
        sportDirectMensShoesPage.sortByPriceRange(30, 60); //Price range
        sportDirectMensShoesPage.checkIfItemsAreFilteredCorrectly(); //Check if all items are correct and sorted correcly
    }

    @Test(dataProvider = "EmailInfo") //uses EmailInfo data provider
    public void checkIfPasswordRecoveryWorks(String email, String password)
    {
        driver.get(webPageUrl);
        commonMethods.checkUrl(webPageUrl);
        commonMethods.closePopUp();
        sportDirectHomePage.goToSignIn();
        commonMethods.checkUrl(webPageUrl + "Login?returnurl=%2f");
        sportDirectSignInPage.goToPasswordRecovery();
        sportDirectSignInPage.fillPasswordRecoveryForm(email);
        driver.get(gmailUrl);
        mailPage.loginMailOpenFirstMessage(email, password);
        mailPage.getHrefGoBackToSportDirect(); //Open last mail and take url
        sportDirectSignInPage.fillNewPassword(password);
        sportDirectSignInPage.openAccount(); //Check if user is logged in
        commonMethods.checkUrl(webPageUrl + "accountinformation");
    }


    @AfterClass // Runs this method after all the test methods in the current class have been run
    public void tearDown() {
        // Close all browser windows and safely end the session
        InitProject.closeBrowser();
    }
}